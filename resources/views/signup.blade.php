<h1>User Signup</h1>
 @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
        </div>
        <img src="images/{{ Session::get('image') }}">
        @endif
  
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

<form action="register" method="post" enctype="multipart/form-data">
<div id="step1">
<label>Name</label>
<input type="text" name="name">
</br>
</br>
<label>Email</label>
<input type="text" name="email">
@csrf
</br>
</br>
<label>Phone</label>
<input type="text" name="phone">
</br>
</br>
</div>
<div id="step2">
<label>Username</label>
<input type="text" name="uname">
</br>
</br>
<label>Password</label>
<input type="password" name="pass">

</br>
</br>
<label>Image</label>
<input type="file" name="image">
</br>
</br>

<label>Google coordinate</label>
<input type="text" name="lat">
<input type="text" name="long">
</div>

<button type="submit" name="submit">Submit</button>

</form>