@extends('blogs.layout')
 
@section('content')
    <div class="row">
	
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Check all Blogs</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('blogs.create') }}"> Create new blogs</a>
            </div>
			 
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Title</th>
            <th>Description</th>
            <th width="250px">Action</th>
        </tr>
        @foreach ($blogs as $blog)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $blog->title }}</td>
            <td>{{ $blog->description }}</td>
            <td>
                <form action="{{ route('blogs.destroy',$blog->id) }}" method="POST">
   
                    <a class="btn btn-info" href="{{ route('blogs.show',$blog->id) }}">Show</a>
    
                    <a class="btn btn-primary" href="{{ route('blogs.edit',$blog->id) }}">Edit</a>
					
				<?php if($blog->status==1) {?>	
				<a class="btn btn-primary" id="user-{{$blog->id}}"  onclick="changeUserStatus({{ $blog->status }}, {{ $blog->id }});">Deactive</a>
				<?php } else {?>
				<a class="btn btn-primary" id="user-{{$blog->id}}"  onclick="changeUserStatus({{ $blog->status }}, {{ $blog->id }});">Active</a>
				<?php } ?>
					 
					
   
                   @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
  
    {!! $blogs->links() !!}
      
@endsection


<script>
function changeUserStatus(status, id) {
	
	
    let _token = $('meta[name="csrf-token"]').attr('content');
	
	//alert(status); 
	//return false;
	
	if(status===1){
		
		//alert('sss');
		
		status=0;
	}  
	else if(status===0){
		
		//alert('sssqqq');
		status=1;
		
	}
   alert(status);
    $.ajax({
        url: 'chnage',
        type: 'post',
		header:{
          'X-CSRF-TOKEN': _token
        },
        data: {
            _token: _token,
            id: id,
            status: status,
			dataType: 'json', 
			contentType:'application/json',	
        },
        success: function (result) {
			
			
			$('#user-'+5).text('sssss');
			
        }
    });
}

</script>