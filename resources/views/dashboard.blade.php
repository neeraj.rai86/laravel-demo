<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        
        <link href="{{url('assets/css/styles.css')}}" rel="stylesheet" />
        <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <!-- Navbar-->
            <ul class="navbar-nav ml-auto ml-md-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                        <a class="dropdown-item" href="#">Settings</a><a class="dropdown-item" href="#">Activity Log</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{url('logout')}}">Logout</a>
                    </div>
                </li>
            </ul>
        </nav>
        <div id="layoutSidenav">
            
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Dashboard</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                        <div class="row">
                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-primary text-white mb-4">
                                    <div class="card-body">@foreach ($records as $rec)

<p>Name:{{ $rec->uname  }}</p>
<p>Phone:{{ $rec->phone  }}</p>
<p>Phone:{{ $rec->email  }}</p>
<p>Image:<img src="images/{{ $rec->img_name }}" ></p>
<p>Lat:{{ $rec->lat  }}</p>
<p>Long:{{ $rec->longi  }}</p>
@endforeach
</div>
<style type="text/css">
body { font: normal 14px Verdana; }
h1 { font-size: 24px; }
h2 { font-size: 18px; }
#sidebar { float: right; width: 30%; }
#main { padding-right: 15px; }
.infoWindow { width: 220px; }
</style>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
//<![CDATA[

var map;

// Ban Jelačić Square - Center of Zagreb, Croatia
var center = new google.maps.LatLng(20.5937, 78.9629);

function init() {

var mapOptions = {
zoom: 5,
center: center,
mapTypeId: google.maps.MapTypeId.ROADMAP
}

//map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);

/* var marker = new google.maps.Marker({
map: map,
position: center,
}); */



var passedArray = <?php echo json_encode($records); ?>

//alert('<?php print_r($records[0]->id); ?>')
//console.log(passedArray);
//var data = JSON.parse(data.responseText);


var map = map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
        var infowindow = new google.maps.InfoWindow(), marker, lat, lng;
        var json= passedArray;

        for( var o in json ){
			
			//console.log(json[0].lat);

            lat = json[ o ].lat;
            lng=json[ o ].longi;
          //  name= json[ o ].name;
            //email= json[ o ].email;
			
			var boxText = "<div class='mapLocationBox'>";
                boxText += "<h2>" + json[ o ].name + "</h2>" + "<p>" + json[ o ].email + "<br>" + json[ o ].phone +  "</p>";
                boxText += "</div>";

            marker = new google.maps.Marker({
                position: new google.maps.LatLng(lat,lng),
                name:name,
                map: map
            }); 
            google.maps.event.addListener( marker, 'click', function(e){
                //infowindow.setContent( this.name);
				infowindow.setContent(boxText);
                
                infowindow.open( map, this );
            }.bind( marker ) );
        }

 


} 
//]]>
</script>
</head>
<body onload="init();">



<section id="sidebar">
<div id="directions_panel"></div>
</section>

<section id="main">
<div id="map_canvas" style="width: 70%; height: 500px;"></div>
</section>
        
    </body>
</html>