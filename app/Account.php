<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    //
	protected $table = 'registers';
	public $timestamps = false;
}
