<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

use Validator,Redirect,Response;
Use App\User;
Use App\Account;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Session;
use DB;

class AuthController extends Controller
{
    public function index()
    {
        return view('login');
    }  
 
    public function register()
    {
        return view('register');
    }
     
    public function postLogin(Request $request)
    {
        request()->validate([
        'email' => 'required',
        'password' => 'required',
        ]);
 
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            // Authentication passed...
            return redirect()->intended('dashboard');
        }
        return Redirect::to("login")->withSuccess('Oppes! You have entered invalid credentials');
    }
 
    public function postRegister(Request $request)
    {  
        request()->validate([
        'name' => 'required',
        'email' => 'required|email|unique:users',
        'password' => 'required|min:6',
		]);
         
        $data = $request->all();
		
		//$user = new User($data);
		// this way you can fill properties that are not fillable
		//$data['phone'] = 'some value';
		// now you save it to database
		//$user->save();
		
	/* echo '<pre>';
	   print_r($request->file('image'));
		print_r($_FILES);
		
		die ;
		 */
		//DB::enableQueryLog(); // Enable query log
		$check = $this->create($data);
		 
		$uid =   DB::getPdo()->lastInsertId();
		
		$user = new Account;
		
		
			$request->validate([
						'phone' => 'required|min:10|numeric',
					]);
		
		$image = $request->file('image');
				$new_name = rand().'.'.$image->getClientOriginalExtension();
				$image->move(public_path("images"),$new_name);
		
		
			   $user->uname = $data['uname'];
				$user->phone = $data['phone'];
				$user->lat = $data['lat'];
				$user->email = $data['email'];
				$user->longi = $data['long'];
				$user->img_name = $new_name;
				$user->uid = $uid;
				
				$user->save();
		
		
// Your Eloquent query executed by using get()

//dd(DB::getQueryLog()); // Show results of log
 return Redirect::to("dashboard")->withSuccess('Great! You have Successfully loggedin');
    }
     
    public function dashboard()
    {
 
      if(Auth::check()){
		  
		  $data = Session::all();
		  
		  
		  
		
		$uid = $data['login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'];
		
		$records = DB::table('registers')->where('uid','=',$uid)->get();
		
	//echo '<pre>';
		//print_r($records); die;
		
     //  return view ('show')->with('records',$records);
        return view('dashboard')->with('records',$records);;
		
		
		
		
      }
       return Redirect::to("login")->withSuccess('Opps! You do not have access');
    }
 
    public function create(array $data)
    {
      return User::create([
        'name' => $data['name'],
        'email' => $data['email'],
        'password' => Hash::make($data['password'])
      ]);
    }
     
    public function logout() {
        Session::flush();
        Auth::logout();
        return Redirect('login');
    }
}
