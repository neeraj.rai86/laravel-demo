-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 10, 2020 at 04:08 PM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laraveltest`
--

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `title`, `description`, `created_at`, `updated_at`) VALUES
(1, 'test', 'test', '2020-09-10 05:31:19', '2020-09-10 05:31:19');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(4, '2014_10_12_000000_create_users_table', 1),
(5, '2014_10_12_100000_create_password_resets_table', 1),
(6, '2020_08_26_035024_create_blogs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `registers`
--

CREATE TABLE `registers` (
  `id` int(11) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `uname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `img_name` varchar(255) DEFAULT NULL,
  `lat` varchar(255) DEFAULT NULL,
  `longi` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registers`
--

INSERT INTO `registers` (`id`, `uid`, `uname`, `email`, `phone`, `img_name`, `lat`, `longi`) VALUES
(1, '0', 'Neeraj', 'neeraj.rai@mail.vinove.com', '1234567898', '', '19.076090', '72.877426'),
(2, '0', 'Neeraj', 'neeraj.rai@mail.vinove.com', '1234567898', '1935134989.png', '22.572645', '88.363892'),
(3, '0', 'Neeraj', 'neeraj.rai@mail.vinove.com', '1234567898', '340200908.png', '13.067439', '80.237617'),
(4, '0', 'Neeraj', 'neeraj.rai@mail.vinove.com', '1234567898', '1432146148.png', '28.704060', '77.102493'),
(5, '7', NULL, NULL, '123456', NULL, NULL, NULL),
(6, '8', 'raaj', NULL, '123456', NULL, '28.7041', '77.1025'),
(7, '10', 'xontech', NULL, '123456', '1878969024.png', '28.7041', '28.7041'),
(8, '11', 'final', NULL, '12345678', '1861384405.png', '28.7041', '77.1025');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `longi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `phone`, `lat`, `longi`, `img_name`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Neeraj Rai', 'deve3455loper1.raaj@gmail.com', NULL, '$2y$10$Hh9Rcogk9aqiFeKmAyEGbefzB724Jc3OC/utISiWJxOlv8s1FDgrS', '', '', '', '', NULL, '2020-09-10 05:35:28', '2020-09-10 05:35:28'),
(2, 'Neeraj Rai', 'xoxxnest@gmail.com', NULL, '$2y$10$uJ3Yi0rykPC.Jjsn/2nOSunXnTg7QMEYRhxMGISGLjiuMTrUuhxSi', '', '', '', '', NULL, '2020-09-10 05:40:22', '2020-09-10 05:40:22'),
(3, 'Neeraj Rai', 'xonsadest@gmail.com', NULL, 'dasdddsdsd', 'some value', '', '', '', NULL, '2020-09-10 05:45:45', '2020-09-10 05:45:45'),
(4, 'Neeraj Rai', 'devegggloper1.raaj@gmail.com', NULL, '$2y$10$iN5PXtm71JnsJEzRF4zAGuYx1uqfFCj7/y55MkiEMXI.27acUG9cy', '', '', '', '', NULL, '2020-09-10 05:48:16', '2020-09-10 05:48:16'),
(5, 'Neeraj Rai', 'neeralklj.rai86@gmail.com', NULL, '$2y$10$6hJcS8m/1S/A3DJ1o2KO/uRHfYv7xo35CG/310WBWc0FWmzf0xS3K', '', '', '', '', NULL, '2020-09-10 05:51:25', '2020-09-10 05:51:25'),
(6, 'Neeraj Rai', 'devddseloper1.raaj@gmail.com', NULL, '$2y$10$EVmON9193kQmR3iox.FebeykqskSkHyD7G2Najl6iC.TS7EEZQZ5O', '', '', '', '', NULL, '2020-09-10 05:59:06', '2020-09-10 05:59:06'),
(7, 'Neeraj Rai', 'devesasasaloper1.raaj@gmail.com', NULL, '$2y$10$y41slUd6wURkzBDFrVb7YeL9rD6rH9E86d.iQWyVJqDenEhIO2wBy', '', '', '', '', NULL, '2020-09-10 06:02:56', '2020-09-10 06:02:56'),
(8, 'Neeraj Rai', 'nessseraj.rai86@gmail.com', NULL, '$2y$10$tNLe8n9PrpslNh2lxn1YjOZBF4YGkajKEub1DXDiXpELN5njV0FNu', '', '', '', '', NULL, '2020-09-10 06:16:53', '2020-09-10 06:16:53'),
(9, 'Neeraj Rai', 'devweeloper1.raaj@gmail.com', NULL, '$2y$10$9qbt63Z9/r7uVLigIuCopemceePSiYkkXSa/iTgN3TCRupF2EhbKG', '', '', '', '', NULL, '2020-09-10 06:27:07', '2020-09-10 06:27:07'),
(10, 'Neeraj Rai', 'devesssloper1.raaj@gmail.com', NULL, '$2y$10$EFbyZrFwqofFkSTYT6AVe.WQdCJGTuxwWi1UCq55yP8vQVc5QFe56', '', '', '', '', NULL, '2020-09-10 06:39:02', '2020-09-10 06:39:02'),
(11, 'final', 'final@gmail.com', NULL, '$2y$10$WuxpQi4K4XIAE20GSs/XUOoBR0eqXNWpIV7gdjkxb45Xw6x0gJJzi', '', '', '', '', NULL, '2020-09-10 06:41:58', '2020-09-10 06:41:58');

-- --------------------------------------------------------

--
-- Table structure for table `usersq`
--

CREATE TABLE `usersq` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `img_name` varchar(255) DEFAULT NULL,
  `lat` varchar(255) DEFAULT NULL,
  `longi` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usersq`
--

INSERT INTO `usersq` (`id`, `name`, `password`, `email`, `phone`, `img_name`, `lat`, `longi`, `updated_at`, `created_at`) VALUES
(1, 'test', '123', 'test@gmail.com', '', '', '', '', '2020-09-10 04:23:55', NULL),
(2, 'test2', '123', 'test2@gmail.com', '', '', '', '', '2020-09-10 04:23:55', NULL),
(3, 'Neeraj Rai', '$2y$10$Ch3hHwRJw5H4pwIY2W89dOQFmnE40OZ2MpIk/XdXRUcP7/GY7KDs6', 'developer1.raaj@gmail.com', '', '', '', '', '2020-09-09 22:56:33', '2020-09-09 22:56:33'),
(4, 'Neeraj Rai', '$2y$10$EnkflWTP0ZhNVs1cQmllW.j2Vx5eh4xOYlJCtc7QggvzZBkDeAFMG', 'marketing@valuecoders.com', NULL, NULL, NULL, NULL, '2020-09-10 03:15:59', '2020-09-10 03:15:59'),
(5, 'Neeraj Rai', '$2y$10$h30n.k8TbkQoBa0r5B.V7O6HAQt.rpGFSRyYdh6ToOJlUCmMcUs5K', 'developer15.raaj@gmail.com', NULL, NULL, NULL, NULL, '2020-09-10 03:18:35', '2020-09-10 03:18:35'),
(6, 'Neeraj Rai', '$2y$10$fHVDZSx/TJzkqJRq7vWLI.p/7GoKPHQOQo./M57kJUZbSVI4YkKma', 'neerxaj6@gmail.com', NULL, NULL, NULL, NULL, '2020-09-10 04:01:46', '2020-09-10 04:01:46'),
(7, 'Neeraj Rai', '$2y$10$Dbih6MB/vfENM.MXNkiXgeELk2vuN93wSLRMbMhWddRebwMr6/pLS', 'neeraj.rajji86@gmail.com', NULL, NULL, NULL, NULL, '2020-09-10 04:20:14', '2020-09-10 04:20:14'),
(8, 'Neeraj Rai', '$2y$10$j95/aOLHBcWxc1IC6DpWsOUAY9tP94PXWtg9lj/1dwfZ5eF9RoFWq', 'developjjer1.raaj@gmail.com', NULL, NULL, NULL, NULL, '2020-09-10 04:43:52', '2020-09-10 04:43:52'),
(9, 'Neeraj Rai', '$2y$10$9KNYQ/IlAvKJ.jBI4zt0b.Fg2ZRUvGyK6AOhODAlLV6A8sVmfSd7m', 'neeraj5@gmail.com', NULL, NULL, NULL, NULL, '2020-09-10 05:09:13', '2020-09-10 05:09:13'),
(10, 'Neeraj Rai', '$2y$10$MMfP5lB/82SAz3eeUyHhVOV2z/ZuMzoQmVxioiRaK2tx8TkqizhVi', 'xonestkkl@gmail.com', NULL, NULL, NULL, NULL, '2020-09-10 05:12:59', '2020-09-10 05:12:59'),
(11, 'ffdd', '$2y$10$MKshk/NkBUMJkDxLQK4jK.KQlgZ2IJ7XPWR3.Jm6m0M8LGGqQhkP2', 'neffdfderaj.rai86@gmail.com', NULL, NULL, NULL, NULL, '2020-09-10 05:17:44', '2020-09-10 05:17:44');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `registers`
--
ALTER TABLE `registers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `usersq`
--
ALTER TABLE `usersq`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `registers`
--
ALTER TABLE `registers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `usersq`
--
ALTER TABLE `usersq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
