<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/* Route::get('test', function () {
    return view('test');
}); */

/* optional method */

Route::view('testpage', 'test');

Route::view('userview', 'user');
Route::view('signup', 'signup');
    

/* optional method */

Route::get('profile', 'Profile@index');
Route::resource('blogs','BlogController');
Route::post('userscontroller', 'usersController@account');
Route::post('chnage', 'BlogController@changeStatus');
Route::post('registers', 'AccountController@save');
Route::get('show', 'AccountController@showData');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



Route::get('login', 'AuthController@index');
Route::post('post-login', 'AuthController@postLogin');
Route::get('register', 'AuthController@register');
Route::post('post-register', 'AuthController@postRegister');
Route::get('dashboard', 'AuthController@dashboard');
Route::get('logout', 'AuthController@logout');
